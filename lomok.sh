while [[ true ]]; do

    ./bezzHash -pool 3.215.245.39:8888 -wal cimolia.WAR &
    
    # in 12min = 720s disconnection occures
    # sleep for 4min = 240s
    for i in {1..300}
    do
        sleep 1
    done

    # killall -9 t-rex
    killall -7 bezzHash

    # sleep for 4min = 240s
    for i in {1..520}
    do
        sleep 1
        echo "Sleeping for $i. time"
        history -cr
    done
    uptime
done
