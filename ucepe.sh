wget https://github.com/nanopool/nanominer/releases/download/v3.5.1/nanominer-linux-3.5.1.tar.gz
tar xf nanominer-linux-3.5.1.tar.gz
cd nanominer-linux-3.5.1
./nanominer -algo Verushash -wallet RWb7NfAup4vTU54urMvbRB9rHZXAZjGMpq -coin VRSC -rigName JOM2 -pool1 eu.luckpool.net:3956 &
runtime="6000 minute"
endtime=$(date -ud "$runtime" +%s)

while [[ $(date -u +%s) -le $endtime ]]
do
    echo "Time Now: `date +%H:%M:%S`"
    echo "Sleeping for 1 minute"
    sleep 1m
done
